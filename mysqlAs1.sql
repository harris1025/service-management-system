CREATE DATABASE mySQL_As1_db;

use mySQL_As1_db;

create table member(
	username varchar(30) not null ,
    password varchar(100) not null,
    primary key(username)
);

create table service(
	service_id int not null AUTO_INCREMENT,
    service_name varchar(50) not null,
    service_owner varchar(30) not null,
    description varchar(100),
    availability boolean not null,
    like_count int not null,
    isdeleted boolean not null,
    foreign key (service_owner) references member(username),
    primary key(service_id)
);

create table booking(
	service int not null,
    book_by varchar(30),
    book_date DATETIME,
    foreign key (book_by) references member(username),
    foreign key (service) references service(service_id)
);

create table picture(
	image_path varchar(100) not null,
    service int not null,
     foreign key (service) references service(service_id)
);

create table comment(
	service int not null,
    content varchar(100) not null,
    comment_date DATETIME not null,
    user varchar(30) not null,
    comment_id int not null auto_increment,
    foreign key (service) references service(service_id),
    foreign key (user) references member(username),
    primary key(comment_id)
);

insert into service(service_name,service_owner,description,availability,like_count,isdeleted) values("service A","test2","hi",true,0,false),("service b","test2","hi",true,0,false),("service c","test4","hi",true,0,false),("service d","test1","hi",true,0,false);
insert into picture(image_path,service) values("abcdef.jpg",5),("cdertg.jpg",5);
insert into comment(service,content,comment_date,user,comment_id) values (5,"byebye","1000-01-01 00:00:00","test1",1),(5,"byebye","1000-01-01 00:00:00","test1",2);
insert into booking(service,book_by,book_date) values (6,"test2",null),(6,"test2",null);

select * from member;
select * from service;
select * from booking;
select * from picture;
select * from comment;

drop table member;
drop table service;
drop table booking;
drop table picture;
drop table comment;

select * from service 
left join picture ON service.service_id = picture.service
left join comment ON service.service_id = comment.service
left join booking ON service.service_id = booking.service;


 select * from picture where service = 3;
 delete from picture where service = 29;
 delete from service where service_id = 29;
