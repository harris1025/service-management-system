const express = require('express');
const multer = require('multer');
const mysql = require('mysql2');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const e = require('express');
const app = express();
const upload = multer({ dest: "./public" });
app.use(express.json());
app.use("/member", auth);
const secret = "88888";

const pool = mysql.createPool({
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'mySQL_As1_db'
});

app.get('/', function (req, res) {
    res.send('Hello World')
})


app.get('/test', (req, res) => {
    connection.execute('SELECT * FROM member', (err, rows, fields) => {
        if (!err)
            res.json(rows);
        else
            console.log(err);
    })
});

app.post("/user/register", function (req, res) {
    const username = req.body.username;
    const password = req.body.password;

    const payload = {
        username: username,
    }

    if (!username) {
        return res.json({ message: "please provide username" });
    }

    if (!password) {
        return res.json({ message: "please provide password" });
    }

    pool.execute("select * from member where username = ? ", [username], function (error, results, fields) {
        // console.log(error, results,fields);
        if (error) {
            return res.json({ message: "error" });
        }

        if (results.length >= 1) {
            return res.json({ message: "have other user this username" });
        } else {
            bcrypt.hash(password, 10, function (err, hash) {

                const token = jwt.sign(payload, secret);

                pool.execute("insert into member (username,password) values (?,?)", [username, hash], function (error, results, fileds) {
                    if (error) {

                        res.json(error);
                    }

                    if (results) {
                        console.log(token);
                        res.json({
                            success: true,
                            token: token,
                            memberId: username
                        });
                    }
                })
            })
        }
    })
})

app.post("/user/login", function (req, res) {

    const username = req.body.username;
    const password = req.body.password;

    let loginResult = {
        success: false
    }

    const payload = {
        username: username,
    }

    const token = jwt.sign(payload, secret);
    console.log(token);

    if (!username) {
        return res.json({ message: "please provide user" });
    }

    if (!password) {
        return res.json({ message: "please provide password" });
    }


    pool.execute("select * from member where username = ? ", [username], function (error, results, fields) {
        // console.log(results);

        if (error) {
            return res.json({ message: "error" });
        }

        if (results.length === 0) {
            return res.json({ message: "cannot find the user" });
        } else {
            bcrypt.compare(password, results[0].password).then(result => {
                if (result) {
                    loginResult.success = true;
                    loginResult.token = token;
                } else {
                    loginResult.success = false;
                    loginResult.token = "login fail";
                }
                res.json(loginResult);
            })
        }
    })
})

app.post("/user/logout", function (req, res) {

    let logoutResult = {
        success: true
    }

    return res.json(logoutResult);

})

function getAvailableService(name, serviceId, description) {
    return {
        name: name,
        serviceid: serviceId,
        description: description
    }
}

app.get("/user/service/available", function (req, res) {
    pool.execute("select * from service where availability = ? and isdeleted = ?", [true, false], function (error, results, fields) {
        console.log(results);
        let output = [];

        if (error) {
            res.json({ message: "error" });
        }

        if (results.length === 0) {
            return res.json(output);
        } else {
            for (let i = 0; i < results.length; i++) {
                output.push(getAvailableService(results[i].service_name, results[i].service_id, results[i].description));
            }
        }
        res.json(output);
    })
})

app.get("/user/service", function (req, res) {
    let name = req.query.service;
    let output = [];

    console.log(name);

    pool.execute("select * from service where availability = ? and isdeleted = ? and service_name like ?", [true, false, "%" + name + "%"], function (error, results, fields) {
        if (results.length === 0) {
            return res.json(output);
        } else {
            for (let i = 0; i < results.length; i++) {
                output.push(getAvailableService(results[i].service_name, results[i].service_id, results[i].description));
            }
        }
        res.json(output);
    })
})


app.get("/user/service/:serviceId/detail", function (req, res) {
    const serviceId = req.params.serviceId;
    const output = [];
    const picResult = [];

    pool.execute("select * from service left join picture ON service.service_id = picture.service left join comment ON service.service_id = comment.service left join booking ON service.service_id = booking.service where availability = ? and service_id = ? and isdeleted = ?", [true, serviceId, 0], function (error, results, fields) {
        if (results.length === 0) {
            return res.json(output);
        }

        const resultService = results.reduce((result, service) => {
            if (Object.keys(result).length === 0) {
                result.serviceId = service.service_id;
                result.serviceName = service.service_name;
                result.service_owner = service.service_owner;
                result.description = service.description;
                result.availability = service.availability;
                result.like_count = service.like_count;
                result.isdeleted = service.isdeleted;
                result.imagePath = [];
                result.comments = [];
            }

            if (service.image_path !== null) {
                picResult.push(service.image_path);
            }

            // result.imagePath.push(service.image_path);

            // console.log(result.comments.findIndex(service.comment_id));

            console.log(service);

            let findComment = result.comments.findIndex((comment) => {
                return service.comment_id === comment.commentId;
            })

            //remarks
            if (findComment === -1 && service.comment_id !== null) {
                result.comments.push({
                    content: service.content,
                    date: service.comment_date,
                    postUser: service.user,
                    commentId: service.comment_id
                })
            }
        
            return result;
        }, {});


        resultService.imagePath = [...new Set(picResult)];
        return res.json(resultService);
    })
})

function auth(req, res, next) {
    const token = req.header('Authorization').replace('Bearer ', '');
    const secret = "88888";

    jwt.verify(token, secret, (err, decoded) => {
        if (err) {
            console.log(err);
            res.status(401).json({
                message: "You have error problem"
            })
        } else {
            req.member = decoded;
            next();
        }
    });
}

app.post("/member/service", upload.array("pic"), function (req, res) {
    const name = req.body.name;
    const description = req.body.description;
    const availability = req.body.availability;
    const pictures = req.files.filename;
    const picArray = req.files.map(file => {
        return file.path;
    });

    if (!name) {
        res.json({ message: "please provide service name" });
    }

    if (!description) {
        res.json({ message: "please provide description" });
    }

    if (!availability) {
        res.json({ message: "please provide availability" });
    }

    pool.execute("insert into service (service_name,description, availability,service_owner,like_count,isdeleted) value (?,?,?,?,?,?)", [name, description, availability, req.member.username, 0, 0], function (error, results, fields) {

        if (error) {
            console.log(error);
            return res.json({ success: false });
        }

        // console.log(results.insertId);

        const serviceId = results.insertId;
        const pathsAndId = [];
        const questionMarks = [];
        const insertSql = "insert into picture (image_path,service) values ";

        for (let i = 0; i < req.files.length; i++) {
            // console.log(req.files);
            questionMarks.push("(?,?)");
            pathsAndId.push(req.files[i].path)
            pathsAndId.push(serviceId)
        }

        const a = insertSql + questionMarks.join(",");
        console.log(a, pathsAndId);

        pool.execute(a, pathsAndId, function (error, results, fields) {
            // console.log(a, pathsAndId,results);

            if (error) {
                return res.json({ success: false })
            }

            return res.json({
                success: true,
                serviceId: serviceId
            })
        });

    })
})

app.patch("/member/service/:serviceId", upload.array("pic"), function (req, res) {
    const serviceId = req.params.serviceId;
    const name = req.body.name;
    const description = req.body.description;
    const availability = req.body.availability;
    const pictures = req.files.filename;
    const picArray = req.files.map(file => {
        return file.path;
    });

    if (!name) {
        return res.json({ message: "please provide service name" });
    }

    if (!description) {
        return res.json({ message: "please provide description" });
    }

    if (!availability) {
        return res.json({ message: "please provide availability" });
    }

    if (req.files.length === 0) {
        return res.json({ message: "please provide pictures" });
    }

    pool.execute("select * from service where service_id = ? and isdeleted = ?", [serviceId, 0], function (error, results, fields) {
        // console.log(results);

        if (results.length === 0) {
            return res.json({ success: "false" })
        }

        if (results[0].service_owner === req.member.username) {
            pool.execute("update service set service_name = ?,description = ?,availability = ? where service_id = ?", [name, description, availability, serviceId], function (error, results, fileds) {
                if (error) {
                    console.log(error);
                    return res.json({ success: false });
                }

                pool.execute("select * from picture where service = ?", [serviceId], function (error, results, fields) {
                    const serviceid = results.insertId;
                    const pathsAndId = [];
                    const questionMarks = [];
                    const insertSql = "insert into picture (image_path,service) values ";

                    if (results.length >= 1) {
                        pool.execute("delete from picture where service=?", [req.params.serviceId]);

                        for (let i = 0; i < req.files.length; i++) {
                            // console.log(req.files);
                            questionMarks.push("(?,?)");
                            pathsAndId.push(req.files[i].path)
                            pathsAndId.push(req.params.serviceId)
                        }

                        const a = insertSql + questionMarks.join(",");
                        // console.log(123456);
                        // console.log(123456,req.params.serviceid);
                        console.log(a, pathsAndId);

                        pool.execute(a, pathsAndId, function (error, results, fields) {
                            if (error) {
                                return res.json({ message: "error" })
                            }
                            return res.json({ success: true })
                        })
                    } else {
                        for (let i = 0; i < req.files.length; i++) {
                            // console.log(req.files);
                            questionMarks.push("(?,?)");
                            pathsAndId.push(req.files[i].path)
                            pathsAndId.push(req.params.serviceId)
                        }

                        const a = insertSql + questionMarks.join(",");

                        console.log(123456);
                        console.log(a, pathsAndId);

                        pool.execute(a, pathsAndId, function (error, results, fields) {
                            if (error) {
                                return res.json({ message: "error" })
                            }
                            return res.json({ success: true })
                        })
                    }
                })
            })
        } else {
            return res.json({
                success: false,
                message: "you are not service owner"
            })
        }

    })
})

app.delete("/member/service/:serviceId", async function (req, res) {
    const serviceId = req.params.serviceId;

    // let [result, fields] = awwait pool.execute();

    pool.execute("select * from service where service_id = ? and isdeleted = ?", [serviceId, 0], function (error, results, fields) {

        if (results.length === 0) {
            return res.json({ message: "no service" });
        }

        if (results[0].service_owner === req.member.username) {

            pool.execute("delete from comment where service = ?", [serviceId], function (error, results, fields) {
                if (error) {
                    return res.json({ message: "error" })
                }

                pool.execute("delete from picture where service = ?", [serviceId], function (error, results, fields) {

                    if (error) {
                        return res.json({ message: "error" })
                    }

                    pool.execute("update service set isdeleted = ? where service_id = ?", [1, serviceId], function (error, results, fields) {

                        if (error) {
                            return res.json({ message: "error" })
                        }

                        return res.json({ success: true });
                    });
                });
            });
        } else {
            return res.json({ success: false });
        }
    })
})


app.patch("/member/service/:serviceId/like", function (req, res) {
    const serviceId = req.params.serviceId;

    pool.execute("select * from service where service_id = ? and isdeleted = ?", [serviceId, 0], function (error, results, fields) {

        if (results.length === 0) {
            return res.json({ message: "cannot find match serviceId" });
        } else {
            const likeCount = results[0].like_count;
            pool.execute("update service set like_count = ? where service_id = ?", [likeCount + 1, serviceId]);
            return res.json({
                success: true,
                likeCount: likeCount + 1
            })
        }
    })
})

//ok
app.post("/member/service/:serviceId/comment", function (req, res) {
    const serviceId = req.params.serviceId;
    const content = req.body.content;
    const dateTime = new Date().toISOString();
    const noRealDate = dateTime.replace("T", " ").replace(".", " ").split(" ", 2).toString();
    const date = noRealDate.replace(",", " ");

    console.log(serviceId, content, date, req.member.username);

    pool.execute("select * from service where service_id = ? and isdeleted = ?", [serviceId, 0], function (error, results, fields) {
        if (results.length === 0) {
            return res.json({ message: "cannot find match serviceId" })
        } else {
            pool.execute("insert into comment (service,content,comment_date,user) values (?,?,?,?)", [req.params.serviceId, content, date, req.member.username], function (error, results, fields) {
                return res.json({ success: true })
            });
        }
    })
})


app.delete("/member/service/:serviceId/booking", function (req, res) {
    const serviceId = req.params.serviceId;

    pool.execute("select * from service where service_id = ? and isdeleted = ?", [serviceId, 0], function (error, results, fields) {
        if (results.length === 0) {
            return res.json({ message: "cannot find match serviceId" })
        }

        if (results[0].service_owner === req.member.username) {
            pool.execute("update service set availability = ? where service_id = ?", [1, serviceId], function (error, result, fields) {
                return res.json({ success: true })
            });
        } else {
            return res.json({ success: false })
        }
    })
})

app.patch("/member/service/:serviceid/availability", function (req, res) {
    const serviceId = req.params.serviceid;
    const availability = req.body.availability;
    const bookedBy = req.body.bookedBy;
    const dateTime = new Date().toISOString();
    const noRealDate = dateTime.replace("T", " ").replace(".", " ").split(" ", 2).toString();
    const date = noRealDate.replace(",", " ");

    console.log(serviceId, availability, bookedBy, date);

    pool.execute("select * from service where service_id = ? and isdeleted = ?", [serviceId, 0], function (error, results, fileds) {

        if (results.length === 0) {
            return res.json({ message: "cannot find match service" })
        }

        if (results[0].service_owner === req.member.username) {

            if (availability === false || availability === 0) {
                pool.execute("insert into booking (service,book_by,book_date) values (?,?,?)", [serviceId, bookedBy, date], function (error, result, fields) {
                    if (error) {
                        return res.json({ message: "error" })
                    }
                    pool.execute("update service set availability = ? where service_id = ? ", [0, serviceId], function (error, results, fileds) {
                        if (error) {
                            return res.json({ message: "error" })
                        }

                        return res.json({ success: true })
                    });
                });
            }

            if (availability === true || availability === 1) {
                pool.execute("update service set availability = ? where service_id = ? ", [1, serviceId], function (error, results, fileds) {
                    if (error) {
                        return res.json({ message: "error" })
                    }
                    return res.json({ success: true })
                });
            }
        } else {
            return res.json({ message: "you are not owner" });
        }

    })
})

app.post("/member/service/:serviceId/booking", function (req, res) {
    const serviceId = req.params.serviceId;
    const dateTime = new Date().toISOString();
    const noRealDate = dateTime.replace("T", " ").replace(".", " ").split(" ", 2).toString();
    const date = noRealDate.replace(",", " ");

    console.log(serviceId, req.member.username);

    pool.execute("select * from service where service_id = ? and availability = ? and isdeleted = ?", [serviceId, 1, 0], function (error, results, fileds) {
        if (results.length === 0) {
            return res.json({ message: "no service can booking" });
        }

        if (results[0].service_owner !== req.member.username) {
            pool.execute("update service set availability = ? where service_id = ? ", [0, serviceId], function (error, results, fileds) {
                if (error) {
                    return res.json({ message: "error" })
                }
                pool.execute("insert into booking (book_by,book_date,service) values (?,?,?)", [req.member.username, date, serviceId], function (error, result, fields) {
                    if (error) {
                        return res.json({ message: "error" })
                    }

                    return res.json({ success: true });
                });
            })
        } else {
            return res.json({ message: "no service can booking" });
        }
    })
})

app.listen(8888);
